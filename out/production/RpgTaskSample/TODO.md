### TODO 

#### Refactor / OOP:
(See `/spells`, already refactored)
- [x] Characters (metadata -> Abstractions)
- [x] Armor
- [x] Weapons
- [ ] WeaponFactory

#### Character requirements
Run all tests in `Main.java`.
Gather all tests in `main.java.demonstrationHelpers` package

- [ ] Can be created and added to party list
- [ ] Default starting equipment (common rarity)
- [ ] attack(). Show: Damage number and type
- [ ] getAttacked(). Show: Incoming phy/mag, resist/dealt

