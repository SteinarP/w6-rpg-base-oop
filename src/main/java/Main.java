package main.java;

import main.java.characters.caster.Mage;
import main.java.consolehelpers.Color;
import main.java.demonstrationHelpers.CharacterTester;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.damaging.ArcaneMissile;

public class Main {
    public static void main(String[] args) {
        System.out.println(Color.RED + "RED COLORED" + Color.RESET + " NORMAL");

        CharacterTester ct = new CharacterTester();
        //ct.runTests();

        ct.testPaladinGear();
        ct.testFightDragon();
        ct.testSpellCasting();
    }
}
