package main.java.items.abstractions;

import main.java.basestats.WeaponStatsModifiers;
import main.java.consolehelpers.Color;

public abstract class Item {
    // Stat modifiers
    private ItemRarity rarity;
    public ItemRarity getRarity() {
        return rarity;
    }

    public double getRarityModifier(){
        switch (rarity){
            default:
            case Common: return 1.0;
            case Uncommon: return 1.1;
            case Rare: return 1.2;
            case Epic: return 1.3;
            case Legendary: return 1.4;
        }
    }

    public String getColor(){
        switch (rarity){
            default:
            case Common: return Color.WHITE;
            case Uncommon: return Color.GREEN;
            case Rare: return Color.BLUE;
            case Epic: return Color.MAGENTA;
            case Legendary: return Color.YELLOW;
        }
    }

    public boolean upgrade(){
        switch (rarity){
            case Common: this.rarity = ItemRarity.Uncommon; return true;
            case Uncommon: this.rarity = ItemRarity.Rare; return true;
            case Rare: this.rarity = ItemRarity.Epic; return true;
            case Epic: this.rarity = ItemRarity.Legendary; return true;
            default: //Can't upgrade maxed legendary.
            return false;
        }
    }

    // Constructors
    public Item(ItemRarity rarity) {
        this.rarity = rarity;
    }
    public Item(){
        this(ItemRarity.Common); //Default
    }
}
