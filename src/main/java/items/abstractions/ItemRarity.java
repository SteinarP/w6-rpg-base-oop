package main.java.items.abstractions;

public enum ItemRarity {
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary
}
