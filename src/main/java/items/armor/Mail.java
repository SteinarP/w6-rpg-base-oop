package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Mail extends Armor {

    // Constructors
    public Mail(ItemRarity rarity) {
        super(rarity);
        type = ArmorType.Mail;
        healthModifier = ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    }
}
