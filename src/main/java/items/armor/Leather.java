package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Leather extends Armor {

    // Constructors
    public Leather(ItemRarity rarity) {
        super(rarity);
        type = ArmorType.Leather;
        healthModifier = ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER;
    }
}
