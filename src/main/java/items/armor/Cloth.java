package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Cloth extends Armor {

    // Constructors
    public Cloth(ItemRarity rarity) {
        super(rarity);
        type = ArmorType.Cloth;
        healthModifier = ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    }
}
