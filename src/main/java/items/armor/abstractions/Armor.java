package main.java.items.armor.abstractions;

import main.java.basestats.ArmorStatsModifiers;
import main.java.characters.abstractions.RpgCharacter;
import main.java.items.abstractions.Item;
import main.java.items.abstractions.ItemRarity;

public abstract class Armor extends Item {
    protected ArmorType type;
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;
    //protected double rarityModifier;'

    // Constructors
    public Armor(ItemRarity itemRarity) {
        super(itemRarity);
    }

    // Public properties
    public ArmorType getType(){ return type; }
    public double getHealthModifier() { return healthModifier; }
    public double getPhysRedModifier() { return physRedModifier; }
    public double getMagicRedModifier() { return magicRedModifier; }
    /*
    public double getRarityModifier() {
        return rarityModifier;
    }*/

    public double getTotalHealthModifier(){ return healthModifier * getRarityModifier();}
    public double getTotalPhysRedModifier(){ return physRedModifier * getRarityModifier();}
    public double getTotalMagicRedModifier(){ return magicRedModifier * getRarityModifier();}

    @Override
    public String toString() {
        String buf = getColor() + getRarity() + " " + getClass().getSimpleName() + "\033[0m (";
        buf += String.format("%.2f", getTotalHealthModifier()) +" HP, ";
        buf += String.format("%.2f", getTotalPhysRedModifier()) +" Phy, ";
        buf += String.format("%.2f", getTotalMagicRedModifier()) + " Mag)";
        return buf + "\033[0m";
    }


}
