package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Plate extends Armor {

    // Constructors
    public Plate(ItemRarity rarity) {
        super(rarity);
        type = ArmorType.Plate;
        healthModifier = ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    }
}
