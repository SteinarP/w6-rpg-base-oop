package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Wand extends Weapon {
    // Constructors
    public Wand(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.WAND_MAGIC_MOD, WeaponCategory.Magic);
    }
}
