package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Staff extends Weapon {
    // Constructors
    public Staff(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.STAFF_MAGIC_MOD, WeaponCategory.Magic);
    }
}
