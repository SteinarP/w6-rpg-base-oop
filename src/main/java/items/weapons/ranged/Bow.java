package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Bow extends Weapon {
    // Constructors
    public Bow(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.BOW_ATTACK_MOD, WeaponCategory.Ranged);
    }
}
