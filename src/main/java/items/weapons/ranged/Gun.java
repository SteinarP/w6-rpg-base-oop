package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Gun extends Weapon {
    // Constructors
    public Gun(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.GUN_ATTACK_MOD, WeaponCategory.Ranged);
    }
}
