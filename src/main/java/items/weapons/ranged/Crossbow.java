package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Crossbow extends Weapon {
    // Constructors
    public Crossbow(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.CROSSBOW_ATTACK_MOD, WeaponCategory.Ranged);
    }
}
