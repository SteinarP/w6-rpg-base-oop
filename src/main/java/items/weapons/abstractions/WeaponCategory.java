package main.java.items.weapons.abstractions;

public enum WeaponCategory {
    Magic,
    Blade,
    Blunt,
    Ranged
}