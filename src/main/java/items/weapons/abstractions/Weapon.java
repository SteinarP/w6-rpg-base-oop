package main.java.items.weapons.abstractions;

import main.java.items.abstractions.Item;
import main.java.items.abstractions.ItemRarity;

public abstract class Weapon extends Item {
    private double powerModifier;
    private WeaponCategory category;

    public double getPowerModifier(){return powerModifier;}
    public WeaponCategory getCategory(){return category;}

    /*
    public Weapon(ItemRarity rarity, double powerModifier){
        super(rarity);
        this.powerModifier = powerModifier;
    }*/

    public Weapon(ItemRarity rarity, double powerModifier, WeaponCategory category){
        super(rarity);
        this.category = category;
        this.powerModifier = powerModifier;
    }

    public double getTotalWeaponModifier(){
        return powerModifier * getRarityModifier();
    }

    @Override
    public String toString() {
        String buf = super.getColor()+super.getRarity() + " ";
        buf += this.getClass().getSimpleName();
        buf += " ("+String.format("%.2f", getTotalWeaponModifier()) + " " + category+")";
        return buf + "\033[0m";
    }
}
