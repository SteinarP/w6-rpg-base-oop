package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Dagger extends Weapon {
    // Constructors
    public Dagger(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.DAGGER_ATTACK_MOD, WeaponCategory.Blade);
    }
}
