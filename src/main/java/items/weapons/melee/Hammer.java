package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Hammer extends Weapon {
    // Constructors
    public Hammer(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.HAMMER_ATTACK_MOD, WeaponCategory.Blunt);
    }
}
