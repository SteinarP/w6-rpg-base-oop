package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Sword extends Weapon {
    // Constructors
    public Sword(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.SWORD_ATTACK_MOD, WeaponCategory.Blade);
    }
}
