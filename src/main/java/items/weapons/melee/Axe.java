package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Axe extends Weapon {
    // Constructors
    public Axe(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.AXE_ATTACK_MOD, WeaponCategory.Blade);
    }
}
