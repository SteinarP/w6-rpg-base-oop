package main.java.spells.abstractions;

/**
 * Task text:
 * "SpellType enum:
 * This was my bad. THe SpellType enum exists to list
 * all the spells currently in the game.
 * This is for creation purposes with the SpellFactory.
 * It should change to:
 * <Same as before except, but added:>
 * IronBark
 * Swiftment
 * <instead of>
 * FlashHeal
 * IronBark
 */

public enum SpellType {
    ArcaneMissile,
    Barrier,
    ChaosBolt,
    FlashHeal,
    IronBark,
    Regrowth
}
