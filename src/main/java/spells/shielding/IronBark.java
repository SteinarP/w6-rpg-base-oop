package main.java.spells.shielding;

import main.java.basestats.SpellModifiers;
import main.java.spells.abstractions.ShieldingSpell;

// Old code mentions both the shielding spells "Rapture" and "IronBark".
// Refactored this Rapture to IronBark to better fit druid's thematic flavor.

public class IronBark implements ShieldingSpell {

    @Override
    public double getAbsorbShieldPercentage() {
        return SpellModifiers.IRONBARK_SHIELD_OF_MAX_HEALTH;
    }

    @Override
    public String getSpellName() {
        return "IronBark";
    }
}
