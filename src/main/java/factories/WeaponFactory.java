package main.java.factories;
// Imports
import main.java.items.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/*
 This factory exists to be responsible for creating new weapons.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {

    public static Weapon get(WeaponType weaponType){
        return get(weaponType, ItemRarity.Common); //Default
    }

    public static Weapon get(WeaponType weaponType, ItemRarity rarity) {
        switch(weaponType) {
            case Axe:
                return new Axe(rarity);
            case Bow:
                return new Bow(rarity);
            case Crossbow:
                return new Crossbow(rarity);
            case Dagger:
                return new Dagger(rarity);
            case Gun:
                return new Gun(rarity);
            case Hammer:
                return new Hammer(rarity);
            case Mace:
                return new Mace(rarity);
            case Staff:
                return new Staff(rarity);
            case Sword:
                return new Sword(rarity);
            case Wand:
                return new Wand(rarity);
            default:
                return null;
        }
    }
}
