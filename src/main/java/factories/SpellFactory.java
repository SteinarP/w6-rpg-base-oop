package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.IronBark;

// TODO Fill in the blanks using the given spells
public class SpellFactory {
    public static Spell get(SpellType spellType) {
        switch(spellType) {
            case Barrier:
                return new Barrier();
            case IronBark:
                return new IronBark();
            case Regrowth:
                return new Regrowth();
            case ChaosBolt:
                return new ChaosBolt();
            case FlashHeal:
                return new SwiftMend(); //Inconsistent naming
            case ArcaneMissile:
                return new ArcaneMissile();
            default:
                return null;
        }
    }
}
