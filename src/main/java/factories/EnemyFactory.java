package main.java.factories;
// Imports
import main.java.enemies.*;
import main.java.enemies.abstractions.Enemy;
import main.java.enemies.abstractions.EnemyType;
/*
 This factory exists to be responsible for creating new enemies.
*/
public class EnemyFactory {
    public static Enemy get(EnemyType enemyType, double difficultyModifier, double floorLevel){
        switch(enemyType) {
            case Beast:
                return new Beast(difficultyModifier, floorLevel);
            case Dragon:
                return new Dragon(difficultyModifier, floorLevel);
            case Elemental:
                return new Elemental(difficultyModifier, floorLevel);
            case Fiend:
                return new Fiend(difficultyModifier, floorLevel);
            case Giant:
                return new Giant(difficultyModifier, floorLevel);
            case Humanoid:
                return new Humanoid(difficultyModifier, floorLevel);
            case Orc:
                return new Orc(difficultyModifier, floorLevel);
            case Undead:
                return new Undead(difficultyModifier, floorLevel);
            default:
                return null;
        }
    }
}
