package main.java.factories;
// Imports

import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.caster.*;
import main.java.characters.melee.*;
import main.java.characters.ranged.*;
import main.java.spells.abstractions.*;

/*
 This factory exists to be responsible for creating new RPG Characters.
 Object is replaced with RpgCharacter as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public static RpgCharacter get(CharacterType characterType) {
        Spell spell;
        switch(characterType) {
            case Mage:
                spell = SpellFactory.get(SpellType.ArcaneMissile);
                return new Mage((DamagingSpell) spell); // Default spell
            case Druid:
                spell = SpellFactory.get(SpellType.Regrowth);
                return new Druid((HealingSpell) spell); // Default spell
            case Rogue:
                return new Rogue();
            case Priest:
                spell = SpellFactory.get(SpellType.Barrier);
                return new Priest((ShieldingSpell) spell); // Default spell
            case Ranger:
                return new Ranger();
            case Paladin:
                return new Paladin();
            case Warlock:
                spell = SpellFactory.get(SpellType.ChaosBolt);
                return new Warlock((DamagingSpell) spell); // Default spell
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}
