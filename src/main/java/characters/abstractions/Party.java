package main.java.characters.abstractions;

import java.util.ArrayList;
import java.util.Iterator;

public class Party implements Iterable<RpgCharacter>{
    final int MAX_PARTY_SIZE = 4;
    ArrayList<RpgCharacter> members = new ArrayList<>(MAX_PARTY_SIZE);

    public Party(){
        this.members = new ArrayList<>(MAX_PARTY_SIZE);
    }

    public void add(RpgCharacter member){
        if (members.size() >= MAX_PARTY_SIZE){
            System.out.println("Can't add "+member.CHAR_TYPE+". Max party size reached! ("+MAX_PARTY_SIZE+")");
            return;
        }
        members.add(member);
    }

    public RpgCharacter get(int index){
        if (index < 0 || index >= MAX_PARTY_SIZE){
            System.err.println("Request invalid party index: "+index);
            return null;
        }
        return members.get(index);
    }

    public void remove(int index){
        members.remove(index);
    }
    public int size(){
        return members.size();
    }

    @Override
    public String toString() {
        if (members == null || members.size()==0){
            return "Nobody";
        }
        String buf = "RPG party of "+members.size()+":\n";
        for (RpgCharacter character : members){
            buf += character.toString() +"\n";
        }
        return buf;
    }

    public String overview(){
        if (members == null || members.size()==0){
            return "";
        }
        String buf = "";
        for (RpgCharacter character : members){
            buf += character.healthBar() + " | " + character.equipment()+"\n";
        }
        return buf;
    }

    public void clear(){
        members.clear();
    }

    @Override
    public Iterator<RpgCharacter> iterator(){
        return members.iterator();
    }

}
