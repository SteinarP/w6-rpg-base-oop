package main.java.characters.abstractions;

public interface CastingShield {
    double shieldPartyMember(double partyMemberMaxHealth);
}
