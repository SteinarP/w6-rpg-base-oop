package main.java.characters.abstractions;

public interface Ranged {
    double attackWithRangedWeapon();
}
