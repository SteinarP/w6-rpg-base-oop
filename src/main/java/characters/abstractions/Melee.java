package main.java.characters.abstractions;

public interface Melee {
    double attackWithMeleeWeapon();
}
