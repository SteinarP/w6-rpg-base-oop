package main.java.characters.abstractions;

public interface CastingDamage {
    double castDamagingSpell();
    // spellDamage = baseMagicPower * spellDamageModifier * weaponPowerModifier * weaponRarityModifier
}
