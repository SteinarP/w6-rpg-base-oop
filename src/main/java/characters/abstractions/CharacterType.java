package main.java.characters.abstractions;

public enum CharacterType {
    Druid,
    Mage,
    Paladin,
    Priest,
    Ranger,
    Rogue,
    Warlock,
    Warrior
}

