package main.java.characters.abstractions;

import main.java.basestats.CharacterStats;
import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.abstractions.Item;
import main.java.items.armor.Dmg;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.abstractions.WeaponType;

public abstract class RpgCharacter {
    // Metadata
    protected CharacterType CHAR_TYPE;
    protected ArmorType ARMOR_TYPE;
    protected WeaponCategory WEAPON_CATEGORY;

    // Base stats defensive
    protected double baseHealth;
    protected double basePhysReduction;  // Armor
    protected double baseMagicReduction; // Magic armor

    // Base stats offensive
    protected double basePower; //For attacks both physical and magical (one main)

    // Active trackers and flags
    protected double currentHealth;
    protected Boolean isDead = false;
    protected Armor armor;
    protected Weapon weapon;
    protected WeaponType defaultWeapon;

    // CONSTRUCTOR
    public RpgCharacter(CharacterType type){
        //Setup stats
        this.CHAR_TYPE = type;
        CharacterStats stats = new CharacterStats();

        baseHealth = stats.HEALTH.get(CHAR_TYPE);
        basePhysReduction = stats.PHYSICAL_RESIST.get(CHAR_TYPE);
        baseMagicReduction = stats.MAGICAL_RESIST.get(CHAR_TYPE);
        basePower = stats.OFFENSE.get(CHAR_TYPE);

        ARMOR_TYPE = stats.ARMOR.get(CHAR_TYPE);
        WEAPON_CATEGORY = stats.WEAPON.get(CHAR_TYPE);
        defaultWeapon = stats.DEFAULT_WEAPON.get(CHAR_TYPE);

        equipDefaultGear();

        //Set to fully healthy
        healFully();
    }

    public void equipDefaultGear(){
        equipArmor(ArmorFactory.get(ARMOR_TYPE));
        equipWeapon(WeaponFactory.get(defaultWeapon));
    }

    // Public getters statuses and stats
    public double getBaseHealth() { return baseHealth; }
    public double getCurrentMaxHealth() {
        return baseHealth * ((armor==null) ? 1 : armor.getTotalHealthModifier());
    }
    public double getCurrentHealth() {
        return currentHealth;
    }
    public double getCurrentHealthPercentage(){ return 100*(currentHealth / getCurrentMaxHealth()); }
    public double getBasePower(){ return basePower; }
    public Boolean getDead() {
        return isDead;
    }

    /**
     * Recovers all health
     */
    public void healFully(){
        this.currentHealth = this.getCurrentMaxHealth();
        this.isDead = false;
    }

    public void heal(double healAmount){
        this.currentHealth = Math.min(this.currentHealth + healAmount, getCurrentMaxHealth());
    }

    // Equipment behaviours
    /**
     * Checks if you can legally equip this
     * @param item
     */
    public boolean canEquip(Item item){
        if (item instanceof Weapon){
            return ((Weapon) item).getCategory() == this.WEAPON_CATEGORY;
        }
        else if (item instanceof Armor){
            return ((Armor) item).getType() == this.ARMOR_TYPE;
        }

        //No more equippable items. Interface here?
        System.out.print("Provided item isn't even in the category of equippable things!");
        return false;
    }

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if(canEquip(armor)){
            this.armor = armor; //Equip
        } else { //Can't wear this.
            System.err.println("RpgCharacter type cannot wear this armor type.");
            throw new IllegalArgumentException();
        }
    }

    /**
     * Decided to keep enums for armor and weapon types, as it'd otherwise
     * require a rewrite of equip logic for each and every character:
     *     public void equipArmor(Armor armor) throws IllegalArgumentException {
     *         if(armor instanceof Leather){
     *             this.armor = armor;
     *         } else {
     *             throw new IllegalArgumentException();
     *         }
     *     }
     */

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon.getCategory() == WEAPON_CATEGORY){
            this.weapon = weapon;
        } else { //Can't use this weapon
            System.err.println("RpgCharacter type cannot wear this armor type.");
            throw new IllegalArgumentException();
        }
    }

    /**
     * Makes some common gear appropriate for their armor and weapon type.
     */

    @Override
    public String toString() {
        return CHAR_TYPE +
                "{ ARMOR:" + ARMOR_TYPE +
                ", WEAPON:" + WEAPON_CATEGORY +
                ", baseHP:" + baseHealth +
                ", basePhysRed:" + basePhysReduction +
                ", baseMagicRed:" + baseMagicReduction +
                ", currentHP:" + currentHealth +
                ", isDead=" + isDead +
                ", armor=" + armor +
                ", weapon=" + weapon +
                '}';
    }

    public String healthBar(){
        final int WIDTH = 20;
        double healthFraction = currentHealth / getCurrentMaxHealth();
        int hpBlocks = Math.max(0, (int)(WIDTH * healthFraction));
        String bar = "\033[32m" + "█".repeat(hpBlocks);
        String missing = isDead?"╳":"░";
        bar += "\033[31m" + missing.repeat(WIDTH-hpBlocks) + "\033[0m";
        String hpNums = (int)currentHealth+"/"+(int)+getCurrentMaxHealth()+" HP";
        return "["+bar+"] "+hpNums;
    }

    /**
     * @param dmg
     * @return the fraction this damage type will be reduced.
     */
    public double getDamageReduction(Dmg dmg){
        switch (dmg){
            case Physical: return basePhysReduction * (armor==null?1:armor.getTotalPhysRedModifier());
            case Magical: return baseMagicReduction * (armor==null?1:armor.getTotalMagicRedModifier());
            default: return 0;
        }
    }

    /**
     * Takes damage from an enemy's attack.
     * Return damage taken after damage reductions, based on type of damage.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, Dmg dmgType) {
        if (dmgType == null){
            System.err.println("[!] Missing damage type on takeDamage().");
            throw new IllegalArgumentException();
        }

        double reduction = getDamageReduction(dmgType);
        double resultDamage = Math.max(1, (incomingDamage * (1 - reduction)));
        this.currentHealth -= resultDamage;
        this.isDead = (currentHealth <= 0);

        return resultDamage;
    }

    public double dealDamage(){
        if (isDead) return 0;
        if (this.weapon == null){
            // Unarmed damage. 1/4 of base, 1/2 for blunt based characters.
            if (WEAPON_CATEGORY == WeaponCategory.Blunt){
                return basePower * 0.5;
            } else {
                return basePower * 0.25;
            }
        }
        return basePower * weapon.getTotalWeaponModifier();
    }

    public String equipment(){
        String buf = CHAR_TYPE +" is ";
        buf += (weapon == null) ? "unarmed" : "wielding " + weapon;
        buf += ", wearing " + ((armor == null) ? "nothing" : armor);
        return buf;
    }

    public Weapon getWeapon(){
        return weapon;
    }

    public Armor getArmor(){
        return armor;
    }
}


