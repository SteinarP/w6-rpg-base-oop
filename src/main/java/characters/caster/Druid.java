package main.java.characters.caster;
// Imports
import main.java.characters.abstractions.CastingHeal;
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends RpgCharacter implements CastingHeal {

    private HealingSpell healingSpell;

    // Constructor
    public Druid(HealingSpell healingSpell) {
        super(CharacterType.Druid); //Sets stats

        this.healingSpell = healingSpell;
    }

    /**
     * Cast a spell healing an ally.
     * @return amount healed.
     */
    public double healPartyMember() {
        return healingSpell.getHealingAmount();
    }

}
