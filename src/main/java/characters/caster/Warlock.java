package main.java.characters.caster;
// Imports
import main.java.characters.abstractions.CastingDamage;
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends RpgCharacter implements CastingDamage {

    private DamagingSpell damagingSpell;

    // Constructor
    public Warlock(DamagingSpell damagingSpell) {
        super(CharacterType.Warlock); //Sets stats

        this.damagingSpell = damagingSpell;
    }

    /**
     * Cast a spell dealing damage to an enemy
     * @return damage dealt
     */
    public double castDamagingSpell() {
        if (damagingSpell == null){
            return 0;
        }
        return dealDamage() * damagingSpell.getSpellDamageModifier();
    }
}
