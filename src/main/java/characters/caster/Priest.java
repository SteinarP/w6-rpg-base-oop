package main.java.characters.caster;
// Imports
import main.java.characters.abstractions.CastingShield;
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends RpgCharacter implements CastingShield {

    private ShieldingSpell shieldingSpell;

    // Constructor
    public Priest(ShieldingSpell shieldingSpell) {
        super(CharacterType.Priest); //Sets stats

        this.shieldingSpell = shieldingSpell;
    }

    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth
     */
    public double shieldPartyMember(double partyMemberMaxHealth) {
        return partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage();
    }

}
