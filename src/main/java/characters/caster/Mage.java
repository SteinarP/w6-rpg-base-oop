package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CastingDamage;
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends RpgCharacter implements CastingDamage {

    private DamagingSpell damagingSpell;
    private double baseMagicPower;

    // Constructor
    public Mage(DamagingSpell damagingSpell) {
        super(CharacterType.Mage); //Sets stats

        this.damagingSpell = damagingSpell;
    }

    /**
     * Cast a spell dealing damage to an enemy
     * @return damage dealt
     */
    public double castDamagingSpell() {
        if (damagingSpell == null){
            return 0;
        }
        return dealDamage() * damagingSpell.getSpellDamageModifier();
    }

}
