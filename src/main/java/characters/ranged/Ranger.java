package main.java.characters.ranged;
// Imports
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Ranged;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends RpgCharacter implements Ranged{

    // Constructor
    public Ranger() {
        super(CharacterType.Ranger); //Sets stats
    }

    // RpgCharacter behaviours
    public double attackWithRangedWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }

}
