package main.java.characters.melee;
// Imports
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends RpgCharacter implements Melee {

    // Constructor
    public Rogue() {
        super(CharacterType.Rogue); //Sets stats
    }

    // RpgCharacter behaviours
    public double attackWithMeleeWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }


}
