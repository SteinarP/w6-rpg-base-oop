package main.java.characters.melee;
// Imports
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends RpgCharacter implements Melee {

    // Constructor
    public Warrior() {
        super(CharacterType.Warrior); //Sets stats
    }

    // RpgCharacter behaviours
    public double attackWithMeleeWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }


}
