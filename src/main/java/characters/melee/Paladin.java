package main.java.characters.melee;
// Imports
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends RpgCharacter implements Melee {

    // Constructor
    public Paladin() {
        super(CharacterType.Paladin); //Sets stats
    }

    // RpgCharacter behaviours
    public double attackWithMeleeWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }

}
