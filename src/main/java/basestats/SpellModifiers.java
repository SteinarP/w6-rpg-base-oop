package main.java.basestats;
// This class serves as a single place to balance all the spell stats.
public class SpellModifiers {
    // ArcaneMissile
    public static final double ARCANE_MISSILE_DAMAGE_MODIFIER = 1.8;
    // ChaosBolt
    public static final double CHAOS_BOLT_DAMAGE_MODIFIER = 1.5;
    // FlashHeal
    public static final double SWIFTMEND_HEAL = 120;
    // Regrowth
    public static final double REGROWTH_HEAL = 100;
    // Barrier
    public static final double BARRIER_SHIELD_OF_MAX_HEALTH = 0.5;
    // IronBark
    public static final double IRONBARK_SHIELD_OF_MAX_HEALTH = 0.8;
}
