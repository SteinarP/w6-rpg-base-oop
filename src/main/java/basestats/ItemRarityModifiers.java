package main.java.basestats;

public class ItemRarityModifiers {
    // Common
    public static final double COMMON_RARITY_MODIFIER = 1; //1
    // Uncommon
    public static final double UNCOMMON_RARITY_MODIFIER = 1.1; //1.2
    // Rare
    public static final double RARE_RARITY_MODIFIER = 1.2; //1.4
    // Epic
    public static final double EPIC_RARITY_MODIFIER = 1.3; //1.6
    // Legendary
    public static final double LEGENDARY_RARITY_MODIFIER = 1.4; //2
}
