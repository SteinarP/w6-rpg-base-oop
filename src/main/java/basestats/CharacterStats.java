package main.java.basestats;

import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.abstractions.WeaponType;

import java.util.EnumMap;

public class CharacterStats {
    // General
    public static final int MAX_PARTY_SIZE = 4;

    // Druid
    public static final double DRUID_BASE_HEALTH = 350;
    public static final double DRUID_BASE_PHYS_RED = 0.15;
    public static final double DRUID_BASE_MAGIC_RES = 0.20;
    public static final double DRUID_BASE_OFFENSE = 0;
    // Mage
    public static final double MAGE_BASE_HEALTH = 320;
    public static final double MAGE_BASE_PHYS_RED = 0.10;
    public static final double MAGE_BASE_MAGIC_RES = 0.30;
    public static final double MAGE_BASE_OFFENSE = 140;
    // Paladin
    public static final double PALADIN_BASE_HEALTH = 400;
    public static final double PALADIN_BASE_PHYS_RED = 0.50;
    public static final double PALADIN_BASE_MAGIC_RES = 0.50;
    public static final double PALADIN_BASE_OFFENSE = 100;
    // Priest
    public final double PRIEST_BASE_HEALTH = 320;
    public final double PRIEST_BASE_PHYS_RED = 10.0;
    public final double PRIEST_BASE_MAGIC_RES = 0.40;
    public final double PRIEST_BASE_OFFENSE = 0;
    // Ranger
    public final double RANGER_BASE_HEALTH = 340;
    public final double RANGER_BASE_PHYS_RED = 0.30;
    public final double RANGER_BASE_MAGIC_RES = 0.30;
    public final double RANGER_BASE_OFFENSE = 140;
    // Rogue
    public final double ROGUE_BASE_HEALTH = 350;
    public final double ROGUE_BASE_PHYS_RED = 0.35;
    public final double ROGUE_BASE_MAGIC_RES = 0.40;
    public final double ROGUE_BASE_OFFENSE = 150;
    // Warlock
    public final double WARLOCK_BASE_HEALTH = 350;
    public final double WARLOCK_BASE_PHYS_RED = 0.20;
    public final double WARLOCK_BASE_MAGIC_RES = 0.40;
    public final double WARLOCK_BASE_OFFENSE = 120;
    // Warrior
    public final double WARRIOR_BASE_HEALTH = 380;
    public final double WARRIOR_BASE_PHYS_RED = 0.50;
    public final double WARRIOR_BASE_MAGIC_RES = 0.10;
    public final double WARRIOR_BASE_OFFENSE = 160;

    public EnumMap<CharacterType, Double> HEALTH = new EnumMap<CharacterType, Double>(CharacterType.class);
    public EnumMap<CharacterType, Double> PHYSICAL_RESIST = new EnumMap<CharacterType, Double>(CharacterType.class);
    public EnumMap<CharacterType, Double> MAGICAL_RESIST = new EnumMap<CharacterType, Double>(CharacterType.class);
    public EnumMap<CharacterType, Double> OFFENSE = new EnumMap<CharacterType, Double>(CharacterType.class);

    public EnumMap<CharacterType, ArmorType> ARMOR = new EnumMap<CharacterType, ArmorType>(CharacterType.class);
    public EnumMap<CharacterType, WeaponCategory> WEAPON = new EnumMap<CharacterType, WeaponCategory>(CharacterType.class);
    public EnumMap<CharacterType, WeaponType> DEFAULT_WEAPON = new EnumMap<CharacterType, WeaponType>(CharacterType.class);

    public CharacterStats(){
        //Yep, this is slightly terrible, but less error prone and repetitive to
        //handle stats in superclass RpgCharacter than to set it in each subclass.
        HEALTH.put(CharacterType.Druid, DRUID_BASE_HEALTH);
        HEALTH.put(CharacterType.Mage, MAGE_BASE_HEALTH);
        HEALTH.put(CharacterType.Paladin, PALADIN_BASE_HEALTH);
        HEALTH.put(CharacterType.Priest, PRIEST_BASE_HEALTH);
        HEALTH.put(CharacterType.Ranger, RANGER_BASE_HEALTH);
        HEALTH.put(CharacterType.Rogue, ROGUE_BASE_HEALTH);
        HEALTH.put(CharacterType.Warlock, WARLOCK_BASE_HEALTH);
        HEALTH.put(CharacterType.Warrior, WARRIOR_BASE_HEALTH);

        PHYSICAL_RESIST.put(CharacterType.Druid, DRUID_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Mage, MAGE_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Paladin, PALADIN_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Priest, PRIEST_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Ranger, RANGER_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Rogue, ROGUE_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Warlock, WARLOCK_BASE_PHYS_RED);
        PHYSICAL_RESIST.put(CharacterType.Warrior, WARRIOR_BASE_PHYS_RED);

        MAGICAL_RESIST.put(CharacterType.Druid, DRUID_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Mage, MAGE_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Paladin, PALADIN_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Priest, PRIEST_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Ranger, RANGER_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Rogue, ROGUE_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Warlock, WARLOCK_BASE_MAGIC_RES);
        MAGICAL_RESIST.put(CharacterType.Warrior, WARRIOR_BASE_MAGIC_RES);

        OFFENSE.put(CharacterType.Druid, DRUID_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Mage, MAGE_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Paladin, PALADIN_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Priest, PRIEST_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Ranger, RANGER_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Rogue, ROGUE_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Warlock, WARLOCK_BASE_OFFENSE);
        OFFENSE.put(CharacterType.Warrior, WARRIOR_BASE_OFFENSE);

        ARMOR.put(CharacterType.Druid, ArmorType.Leather);
        ARMOR.put(CharacterType.Mage, ArmorType.Cloth);
        ARMOR.put(CharacterType.Paladin, ArmorType.Plate);
        ARMOR.put(CharacterType.Priest, ArmorType.Cloth);
        ARMOR.put(CharacterType.Ranger, ArmorType.Mail);
        ARMOR.put(CharacterType.Rogue, ArmorType.Leather);
        ARMOR.put(CharacterType.Warlock, ArmorType.Cloth);
        ARMOR.put(CharacterType.Warrior, ArmorType.Plate);

        WEAPON.put(CharacterType.Druid, WeaponCategory.Magic);
        WEAPON.put(CharacterType.Mage, WeaponCategory.Magic);
        WEAPON.put(CharacterType.Paladin, WeaponCategory.Blunt);
        WEAPON.put(CharacterType.Priest, WeaponCategory.Magic);
        WEAPON.put(CharacterType.Ranger, WeaponCategory.Ranged);
        WEAPON.put(CharacterType.Rogue, WeaponCategory.Blade);
        WEAPON.put(CharacterType.Warlock, WeaponCategory.Magic);
        WEAPON.put(CharacterType.Warrior, WeaponCategory.Blade);

        DEFAULT_WEAPON.put(CharacterType.Druid, WeaponType.Staff);
        DEFAULT_WEAPON.put(CharacterType.Mage, WeaponType.Wand);
        DEFAULT_WEAPON.put(CharacterType.Paladin, WeaponType.Hammer);
        DEFAULT_WEAPON.put(CharacterType.Priest, WeaponType.Staff);
        DEFAULT_WEAPON.put(CharacterType.Ranger, WeaponType.Bow);
        DEFAULT_WEAPON.put(CharacterType.Rogue, WeaponType.Dagger);
        DEFAULT_WEAPON.put(CharacterType.Warlock, WeaponType.Wand);
        DEFAULT_WEAPON.put(CharacterType.Warrior, WeaponType.Axe);
    }

}
