package main.java.basestats;
// This class serves as a single place to balance all the stats for armor
public class ArmorStatsModifiers {
    // Cloth
    public static final double CLOTH_HEALTH_MODIFIER = 1;
    public static final double CLOTH_PHYS_RED_MODIFIER = 1;
    public static final double CLOTH_MAGIC_RED_MODIFIER = 2;
    // Leather
    public static final double LEATHER_HEALTH_MODIFIER = 1.1;
    public static final double LEATHER_PHYS_RED_MODIFIER = 1.1;
    public static final double LEATHER_MAGIC_RED_MODIFIER = 1.2;
    // Mail
    public static final double MAIL_HEALTH_MODIFIER = 1.2;
    public static final double MAIL_PHYS_RED_MODIFIER = 1.2;
    public static final double MAIL_MAGIC_RED_MODIFIER = 1.2;
    // Plate
    public static final double PLATE_HEALTH_MODIFIER = 1.5;
    public static final double PLATE_PHYS_RED_MODIFIER = 1.4;
    public static final double PLATE_MAGIC_RED_MODIFIER = 1;
}
