package main.java.basestats;
// This class serves as a single place to balance all the defensive stats of the characters.
public class CharacterBaseStatsDefensive {
    // Druid
    public static final double DRUID_BASE_HEALTH = 350;
    public static final double DRUID_BASE_PHYS_RED = 0.15;
    public static final double DRUID_BASE_MAGIC_RES = 0.20;
    // Mage
    public static final double MAGE_BASE_HEALTH = 320;
    public static final double MAGE_BASE_PHYS_RED = 0.10;
    public static final double MAGE_BASE_MAGIC_RES = 0.30;
    // Paladin
    public static final double PALADIN_BASE_HEALTH = 400;
    public static final double PALADIN_BASE_PHYS_RED = 0.50;
    public static final double PALADIN_BASE_MAGIC_RES = 0.50;
    // Priest
    public static final double PRIEST_BASE_HEALTH = 320;
    public static final double PRIEST_BASE_PHYS_RED = 10.0;
    public static final double PRIEST_BASE_MAGIC_RES = 0.40;
    // Ranger
    public static final double RANGER_BASE_HEALTH = 340;
    public static final double RANGER_BASE_PHYS_RED = 0.30;
    public static final double RANGER_BASE_MAGIC_RES = 0.30;
    // Rogue
    public static final double ROGUE_BASE_HEALTH = 350;
    public static final double ROGUE_BASE_PHYS_RED = 0.35;
    public static final double ROGUE_BASE_MAGIC_RES = 40;
    // Warlock
    public static final double WARLOCK_BASE_HEALTH = 350;
    public static final double WARLOCK_BASE_PHYS_RED = 0.20;
    public static final double WARLOCK_BASE_MAGIC_RES = 0.40;
    // Warrior
    public static final double WARRIOR_BASE_HEALTH = 380;
    public static final double WARRIOR_BASE_PHYS_RED = 0.50;
    public static final double WARRIOR_BASE_MAGIC_RES = 0.10;
}
