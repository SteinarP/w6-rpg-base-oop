package main.java.demonstrationHelpers;

import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Party;
import main.java.characters.abstractions.RpgCharacter;
import main.java.characters.caster.Druid;
import main.java.characters.caster.Priest;
import main.java.characters.caster.Warlock;
import main.java.characters.melee.Paladin;
import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.WeaponFactory;
import main.java.items.abstractions.ItemRarity;
import main.java.items.armor.Dmg;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;

public class CharacterTester {

    private Party party;

    public void createDefaultParty(){
        RpgCharacter paul = CharacterFactory.get(CharacterType.Paladin);
        RpgCharacter roger = CharacterFactory.get(CharacterType.Rogue);
        RpgCharacter robin = CharacterFactory.get(CharacterType.Ranger);
        RpgCharacter mago = CharacterFactory.get(CharacterType.Mage);

        this.party = new Party();
        party.clear();
        party.add(paul);
        party.add(roger);
        party.add(robin);
        party.add(mago);
    }

    public CharacterTester(){
        createDefaultParty();
        System.out.println("Created default party: "+party);
    }

    public void testPaladinGear(){
        this.createDefaultParty();
        System.out.println("\n\n===============================================");
        System.out.println("SIMULATING: PALADIN EQUIPPING GEAR");
        Paladin paul = (Paladin) party.get(0);
        //Equipping weapons and armor
        System.out.println(paul.equipment());
        Weapon sharpStick = WeaponFactory.get(WeaponType.Sword, ItemRarity.Uncommon);
        System.out.println("Can Paul the Paladin equip \""+sharpStick+"\"? --> "+paul.canEquip(sharpStick));

        Weapon smashStick = WeaponFactory.get(WeaponType.Mace, ItemRarity.Legendary);
        System.out.println("Can Paul the Paladin equip \""+smashStick+"\"? --> "+paul.canEquip(smashStick));
        paul.equipWeapon(smashStick);

        Armor mail = ArmorFactory.get(ArmorType.Mail, ItemRarity.Uncommon);
        System.out.println("Can Paul the Paladin equip \""+mail+"\"? --> "+paul.canEquip(mail));
        paul.equipWeapon(smashStick);
        Armor plate = ArmorFactory.get(ArmorType.Plate, ItemRarity.Uncommon);
        System.out.println("Can Paul the Paladin equip \""+plate+"\"? --> "+paul.canEquip(plate));
        paul.equipWeapon(smashStick);
        paul.equipArmor(plate);
        System.out.println(paul.equipment());

        //Attacking and taking damage
        double paulDamage = paul.dealDamage();
        System.out.println("\nPaul swings his "+paul.getWeapon()+" dealing "+paulDamage +" dmg");
        System.out.println("Unfortunately, Paul clumsily misses his target and the weapon swings into his kneecap");
        damageChar(paulDamage, Dmg.Physical, paul);

        Armor betterPlate = ArmorFactory.get(ArmorType.Plate, ItemRarity.Epic);
        System.out.println("\nCan Paul the Paladin equip \""+betterPlate+"\"? --> "+paul.canEquip(betterPlate));
        paul.equipArmor(betterPlate);
        System.out.println(paul.equipment());
        System.out.println("Paul swings his "+paul.getWeapon()+" dealing "+paulDamage +" dmg");
        System.out.println("Unfortunately, Paul clumsily misses his target and the weapon swings into his kneecap");
        damageChar(paulDamage, Dmg.Physical, paul);
    }

    public void damageChar(double damage, Dmg type, RpgCharacter target){
        double resultDamage = target.takeDamage(damage, type);

        //And print it
        String armorName = target.getArmor().toString().split("\\(")[0] +" \033[0m";
        double reduction = target.getDamageReduction(type);
        System.out.println(armorName+": "+(int)damage+" "+type+" - "+(int)(100*reduction)+"% = "
                +(int)resultDamage + (target.getDead()?" \uD83D\uDC80"+ Color.RED +"[DEAD!]"+Color.RESET+"\uD83D\uDC80":""));

    }

    public void damageEveryone(double damage, Dmg type){
        for (RpgCharacter character : party){
            damageChar(damage, type, character);
        }
    }


    public void testFightDragon(){
        this.createDefaultParty();
        System.out.println("\n\n===============================================");
        System.out.println("SIMULATING: EVERYONE TAKING DAMAGE FROM DRAGON");

        System.out.println(party);
        System.out.println(party.overview());

        System.out.println("The dragon exhales a magic inferno at the entire party.");
        damageEveryone(200, Dmg.Magical);
        System.out.println(party.overview());

        System.out.println("The dragon whips its tail, hitting the entire party.");
        damageEveryone(200, Dmg.Physical);
        System.out.println(party.overview());

        System.out.println("A divine intervention upgrades all armor and fully heals the party.");
        for (RpgCharacter character : party){
            Armor am = character.getArmor();
            am.upgrade(); am.upgrade(); am.upgrade(); am.upgrade();
            character.healFully();
        }

        System.out.println("The dragon wants to unit test the Paladin's stupidly strong armor, crunching him in its jaws");
        damageChar(10000, Dmg.Physical, party.get(0));
        System.out.println(party.get(0).healthBar());
        System.out.println("The dragon repeats its fire breath against the legendary armor.");
        damageEveryone(200, Dmg.Magical);
        System.out.println(party.overview());

        System.out.println("Frustrated at the reduced effectiveness, the dragon whips its tail again.");
        damageEveryone(200, Dmg.Physical);
        System.out.println(party.overview());

        System.out.println("The dragon claws the ranger and mage");
        damageChar(300, Dmg.Physical, party.get(2));
        damageChar(300, Dmg.Physical, party.get(3));
        System.out.println(party.overview());
    }

    public void testSpellCasting(){
        System.out.println("\n\n===============================================");
        System.out.println("SIMULATING: MAGIC USERS");
        this.party = new Party(); //empty

        //Add 3 magic users
        Druid druid = (Druid) CharacterFactory.get(CharacterType.Druid);
        party.add(druid);
        Priest priest = (Priest) CharacterFactory.get(CharacterType.Priest);
        party.add(priest);
        Warlock warlock = (Warlock) CharacterFactory.get(CharacterType.Warlock);
        party.add(warlock);

        System.out.println(party.overview());
        double spellDamage = warlock.castDamagingSpell();
        System.out.println("Warlock casts a spell dealing "+spellDamage+" damage");
        System.out.println("Woops, the magic mirror reflects it to the priest.");
        damageChar(spellDamage, Dmg.Magical, priest);
        System.out.println(priest.healthBar());
        double healAmount = druid.healPartyMember();
        System.out.println("Druid heals priest for "+healAmount);
        priest.heal(healAmount);
        System.out.println(priest.healthBar());

        warlock.equipWeapon(WeaponFactory.get(WeaponType.Staff, ItemRarity.Legendary));
        System.out.println("\n\"Hey, I found this plausibly cursed staff\", said the Warlock, waving it experimentally.");
        System.out.println(warlock.equipment());
        double shieldAmount = priest.shieldPartyMember(druid.getCurrentMaxHealth());
        System.out.println("Priest expects the worst, casting shield on Druid, adding "
                +(int)shieldAmount+" temporary HP on top of "+(int)druid.getCurrentHealth() +" (+"+shieldAmount/druid.getCurrentHealth()+" of max)");


        spellDamage = warlock.castDamagingSpell();
        System.out.println("Suddenly the \"plausibly cursed\" staff crackles with uncontrollable energy, sending a chaos bolt on all in the room ("+(int)spellDamage+" dmg)");

        //Simple shield mechanic, ignoring target resistance. Can't bother writing complex temporary HP logic right now.
        System.out.println("Shield @Druid: "+(int)spellDamage+" - "+(int)(shieldAmount)+" = "+(int)(spellDamage-shieldAmount));
        damageChar(spellDamage - shieldAmount, Dmg.Magical, druid);
        damageChar(spellDamage, Dmg.Magical, priest);
        damageChar(spellDamage, Dmg.Magical, warlock);
        System.out.println(party.overview());
    }

    /**
     * - [ ] Can be created and added to party list
     * - [ ] Default starting equipment (common rarity)
     * - [ ] attack(). Show: Damage number and type
     * - [ ] getAttacked(). Show: Incoming phy/mag, resist/dealt
     */
}
