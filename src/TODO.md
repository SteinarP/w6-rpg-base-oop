### TODO 

#### Refactor / OOP:
(See `/spells`, already refactored)
- [x] Characters (metadata -> Abstractions)
- [x] Armor
- [x] Weapons
- [x] WeaponFactory
- [x] All factories

#### Character requirements
Run all tests in `Main.java`.
Gather all tests in `main.java.demonstrationHelpers` package

- [x] Can be created and added to party list
- [x] Default starting equipment (common rarity)
- [x] Equip new gear (demo change)
- [x] deal damage. Show: Damage number and type
- [x] take damage. Show: Incoming phy/mag, resist/dealt

